#!/usr/bin/env pythoh3

# MARTA NÚÑEZ GARCÍA

'''
Program to order a list of words given as arguments
'''

import sys

# Función "is_lower". En primer lugar se comprueba la longitud de las palabras que posteriormente se comparan
# alfabéticamente, ya que de esta manera, el rango del bucle "FOR" implementado posteriormente que itera en cada
# una de las palabras, es distinto según dicha longitud. Se compara cada una de las palabras letra a letra
# (transformándolo en minúsculas). Si son distintas letras, se comprueba la que es anterior alfabéticamente (menor).
# En este caso la función devuelve "True". En caso contrario, devuelve "False".
def is_lower(first: str, second: str):
    """Return True if first is lower (alphabetically) than second

    Order is checked after lowercasing the letters
    `<` is only used on single characteres.
    """
    if len(first) < len(second):
        leng = len(first)
    else:
        leng = len(second)
    for n in range(leng):
        if first[n].lower() != second[n].lower():
            if first[n].lower() < second[n].lower():
                return True
            else:
                return False



# Función "sort_pivot". Comprueba si la palabra es menor que la palabra posicionada en la posición pivote. La palabra
# menor se posiciona a la izquierda de las demás hasta que encuentra una menor (intercambian posiciones).
def sort_pivot(words: list, pos: int):
    """Sorts word in pivot position, moving it to the left until its place"""
    for i in range(pos+1, len(words)):
        if is_lower(words[i], words[pos]) == True:
            words[i], words[pos] = words[pos], words[i]
    return words



# Función "sort". Recorre la lista de palabras proporcionada obteniendo una lista ordenada alfabéticamente a partir de
# la función "sort_pivot".
def sort(words: list):
    """Return the list of words, ordered alphabetically"""
    for m in range(0, len(words)):
        lower = sort_pivot(words, m)
    return lower


# Función "show". Se obtienen strings a partir de la lista proporcionada. Posteriormente, la función devuelve la
# impresión por pantalla de las palabras ordenadas alfabéticamente.
def show(words: list):
    """Show words on screen, using print()"""
    write = " ".join(words)
    imp = print(write)
    return imp


def main():
    words: list = sys.argv[1:]
    sort(words)
    show(words)

if __name__ == '__main__':
    main()